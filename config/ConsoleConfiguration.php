<?php

use PlaceHolderX\Application\Console\Configuration\Parameters;
use PlaceHolderX\Application\Console\Configuration\Services;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;

return static function(ContainerConfigurator $configurator) {
    Parameters::configure($configurator->parameters());
    Services::configure($configurator->services());
};
