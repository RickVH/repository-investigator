# Repository Investigator
This project will allow users to gather additional information from a repository. 
The use case that inspired this project consist of constructing a list of merge requests 
with their status, based on different variables.

**Note:** at the moment only Gitlab is supported

## Configuration

### Required environment variables
The application requires several parameters which can be set through the `.env` file. 
See the `.env.example` file which parameters need to be set. 

### Reviewer pool
If you want to see possible reviewers for the open merge requests, a reviewer pool 
can be configured. To do this see the `config/reviewers.yaml` file need to be added. 
See `config/reviewers.yaml.dist` which contains the example value.

## Current features

### Console commands
The application has a console component that can run commands. The console can be 
executed with:
```shell script
bin/console
```

#### Open merge request overview
Retrieves open merge request for the configured repository. It shows the amount of approvals, 
open discussions and possible reviewers (if configured).  

The command can be executed as followed:
```shell script
bin/console merge-request:overview
```

The command accepts the following options:
- `include-wip` (shortcut: `i`): When given, it will also fetch the merge requests 
that are marked as Work In Progress or is a draft.
- `exclusion-label` (shortcut: `e`): Merge requests with given labels will not be 
shown in the overview. This option can be applied multiple times.