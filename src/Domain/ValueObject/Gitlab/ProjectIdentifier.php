<?php

declare(strict_types=1);

namespace PlaceHolderX\Domain\ValueObject\Gitlab;

final class ProjectIdentifier
{
    private int $value;

    public function __construct(int $value)
    {
        $this->value = $value;
    }

    public static function create(int $value): self
    {
        return new self($value);
    }

    public function value(): int
    {
        return $this->value;
    }
}
