<?php

namespace PlaceHolderX\Domain\Collection;

use PlaceHolderX\Domain\Model\MergeRequest;
use PlaceHolderX\Domain\Model\Reviewer;

final class ReviewerPool
{
    /** @var Reviewer[]  */
    private array $reviewers;

    /**
     * @param Reviewer[] $reviewers
     */
    public function __construct(array $reviewers = [])
    {
        $this->reviewers = [];

        foreach ($reviewers as $reviewer) {
            $this->add($reviewer);
        }
    }

    public function add(Reviewer $reviewer): void
    {
        $this->reviewers[$reviewer->username()] = $reviewer;
    }

    public function eligibleReviewers(MergeRequest $mergeRequest): self
    {
        $eligibleReviewers = [];

        foreach ($this->reviewers as $reviewer) {
            if ($mergeRequest->isCreatedBy($reviewer->username())) {
                continue;
            }

            if ($mergeRequest->isApprovedBy($reviewer->username())){
                continue;
            }

            $eligibleReviewers[] = $reviewer;
        }

        return new self($eligibleReviewers);
    }

    public function toString(): string
    {
        if (\count($this->reviewers) === 0) {
            return 'None';
        }

        $reviewerNames = \array_map(static function(Reviewer $reviewer) {
            return $reviewer->name();
        }, $this->reviewers);

        return implode(', ', $reviewerNames);
    }
}
