<?php

declare(strict_types=1);

namespace PlaceHolderX\Domain;

use PlaceHolderX\Domain\Repository\GitlabMergeRequestRepository;
use RuntimeException;

final class DomainRegistry
{
    private static DomainRegistry $instanceObject;
    private GitlabMergeRequestRepository $gitlabMergeRequestRepository;

    private function __construct(
        GitlabMergeRequestRepository $gitlabMergeRequestRepository
    )
    {
        $this->gitlabMergeRequestRepository = $gitlabMergeRequestRepository;
    }

    public static function initialize(
        GitlabMergeRequestRepository $gitlabMergeRequestRepository
    ): self
    {
        self::$instanceObject = new self(
            $gitlabMergeRequestRepository
        );

        return self::$instanceObject;
    }

    public static function instance(): self
    {
        if (!self::$instanceObject instanceof self) {
            throw new RuntimeException('Registry not initialized');
        }

        return self::$instanceObject;
    }

    public function gitlabMergeRequestRepository(): GitlabMergeRequestRepository
    {
        return $this->gitlabMergeRequestRepository;
    }
}
