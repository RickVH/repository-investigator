<?php

declare(strict_types=1);

namespace PlaceHolderX\Domain\Model;

interface MergeRequest
{
    public function title(): string;
    public function url(): string;
    public function amountOfUnresolvedDiscussions(): int;
    public function amountOfApprovals(): int;
    public function isApprovedBy(string $username): bool;
    public function isCreatedBy(string $username): bool;
}
