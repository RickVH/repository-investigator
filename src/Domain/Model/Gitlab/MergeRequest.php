<?php

declare(strict_types=1);

namespace PlaceHolderX\Domain\Model\Gitlab;

use PlaceHolderX\Domain\Model\MergeRequest as MergeRequestInterface;
use PlaceHolderX\Domain\Model\User;
use PlaceHolderX\Domain\ValueObject\Gitlab\MergeRequestIdentifier;
use PlaceHolderX\Domain\ValueObject\Gitlab\MergeRequestInternalIdentifier;
use PlaceHolderX\Domain\ValueObject\Gitlab\ProjectIdentifier;
use function count;

final class MergeRequest implements MergeRequestInterface
{
    private MergeRequestIdentifier $identifier;
    private MergeRequestInternalIdentifier $internalIdentifier;
    private ProjectIdentifier $projectIdentifier;
    private string $title;
    private string $url;
    private int $unresolvedDiscussionsCount;
    /** @var User[] */
    private array $approvedByUsers;
    private User $createdBy;

    /**
     * @param User[] $approvedByUsers
     */
    public function __construct(
        MergeRequestIdentifier $identifier,
        MergeRequestInternalIdentifier $internalIdentifier,
        ProjectIdentifier $projectIdentifier,
        string $title,
        string $url,
        int $unresolvedDiscussionsCount,
        array $approvedByUsers,
        User $createdBy
    )
    {
        $this->identifier = $identifier;
        $this->internalIdentifier = $internalIdentifier;
        $this->projectIdentifier = $projectIdentifier;
        $this->title = $title;
        $this->url = $url;
        $this->unresolvedDiscussionsCount = $unresolvedDiscussionsCount;
        $this->approvedByUsers = [];
        $this->createdBy = $createdBy;

        foreach ($approvedByUsers as $approvedByUser) {
            $this->addApprovedByUser($approvedByUser);
        }
    }

    public function identifier(): MergeRequestIdentifier
    {
        return $this->identifier;
    }

    public function internalIdentifier(): MergeRequestInternalIdentifier
    {
        return $this->internalIdentifier;
    }

    public function projectIdentifier(): ProjectIdentifier
    {
        return $this->projectIdentifier;
    }

    public function title(): string
    {
        return $this->title;
    }

    public function url(): string
    {
        return $this->url;
    }

    public function amountOfUnresolvedDiscussions(): int
    {
        return $this->unresolvedDiscussionsCount;
    }

    public function amountOfApprovals(): int
    {
        return count($this->approvedByUsers);
    }

    public function isApprovedBy(string $username): bool
    {
        foreach ($this->approvedByUsers as $user) {
            if ($user->username() === $username) {
                return true;
            }
        }

        return false;
    }

    public function isCreatedBy(string $username): bool
    {
        return $this->createdBy()->username() === $username;
    }

    public function createdBy(): User
    {
        return $this->createdBy;
    }

    private function addApprovedByUser(User $user): void
    {
        $this->approvedByUsers[] = $user;
    }
}
