<?php

declare(strict_types=1);

namespace PlaceHolderX\Domain\Model\Gitlab;

use PlaceHolderX\Domain\Model\User as UserInterface;
use PlaceHolderX\Domain\ValueObject\Gitlab\UserIdentifier;

final class User implements UserInterface
{
    private UserIdentifier $identifier;
    private string $username;
    private string $name;

    public function __construct(UserIdentifier $identifier, string $username, string $name)
    {
        $this->identifier = $identifier;
        $this->username = $username;
        $this->name = $name;
    }

    public function identifier(): UserIdentifier
    {
        return $this->identifier;
    }

    public function username(): string
    {
        return $this->username;
    }

    public function name(): string
    {
        return $this->name;
    }
}
