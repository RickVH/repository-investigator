<?php

namespace PlaceHolderX\Domain\Model;

final class Reviewer
{
    private string $username;
    private string $name;

    public function __construct(string $username, string $name)
    {
        $this->username = $username;
        $this->name = $name;
    }

    public function username(): string
    {
        return $this->username;
    }

    public function name(): string
    {
        return $this->name;
    }
}