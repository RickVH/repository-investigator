<?php

declare(strict_types=1);

namespace PlaceHolderX\Domain\Model;

interface User
{
    public function username(): string;
}
