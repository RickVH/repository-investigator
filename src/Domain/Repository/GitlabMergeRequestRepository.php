<?php

declare(strict_types=1);

namespace PlaceHolderX\Domain\Repository;

use PlaceHolderX\Domain\Model\MergeRequest;
use PlaceHolderX\Domain\ValueObject\Gitlab\ProjectIdentifier;

interface GitlabMergeRequestRepository
{
    /**
     * @param string[] $exclusionLabels
     * @return MergeRequest[]
     */
    public function findOpenByProject(
        ProjectIdentifier $projectIdentifier,
        array $exclusionLabels = [],
        bool $includeWip = false
    ): array;
}
