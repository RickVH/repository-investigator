<?php

declare(strict_types=1);

namespace PlaceHolderX\Application\DependencyInjection\CompilerPass;

use Exception;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;


final class ForceInitialisation implements CompilerPassInterface
{
    /**
     * @throws Exception
     */
    public function process(ContainerBuilder $container): void
    {
        $services = $container->findTaggedServiceIds('app.force_initialize');

        foreach(array_keys($services) as $serviceId) {
            $container->get($serviceId);
        }
    }
}
