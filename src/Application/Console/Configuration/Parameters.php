<?php

namespace PlaceHolderX\Application\Console\Configuration;

use Symfony\Component\DependencyInjection\Loader\Configurator\ParametersConfigurator;

final class Parameters
{
    public static function configure(ParametersConfigurator $configurator): void
    {
        $configurator->set('gitlab.private_token', '%env(string:GITLAB_API_PRIVATE_TOKEN)%');
        $configurator->set('gitlab.project_id', '%env(int:GITLAB_PROJECT_ID)%');
        $configurator->set('gitlab.current_user', '%env(string:GITLAB_CURRENT_USER)%');
    }
}