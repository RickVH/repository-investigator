<?php

namespace PlaceHolderX\Application\Console\Configuration;

use GuzzleHttp\Client as GuzzleClient;
use PlaceHolderX\Application\Console\Command\MergeRequestOverview;
use PlaceHolderX\Domain\Collection\ReviewerPool;
use PlaceHolderX\Domain\DomainRegistry;
use PlaceHolderX\Domain\Model\Reviewer;
use PlaceHolderX\Domain\Repository\GitlabMergeRequestRepository;
use PlaceHolderX\Infrastructure\ApiClient\Gitlab\Client as GitlabClientImplementation;
use PlaceHolderX\Infrastructure\ApiClient\GitlabClient;
use PlaceHolderX\Infrastructure\Repository\Api\ApiGitlabMergeRequestRepository;
use Psr\Http\Client\ClientInterface;
use Symfony\Component\Console\Application;
use Symfony\Component\DependencyInjection\Loader\Configurator\ReferenceConfigurator;
use Symfony\Component\DependencyInjection\Loader\Configurator\ServicesConfigurator;
use Symfony\Component\Yaml\Yaml;

final class Services
{
    private const CONFIG_DIRECTORY = __DIR__ . '/../../../../config/';
    private const REVIEWER_FILE = 'reviewers.yaml';

    public static function configure(ServicesConfigurator $configurator): void
    {
        $configurator
            ->defaults()
            ->autowire()
            ->autoconfigure()
            ->bind('$projectId', '%gitlab.project_id%')
            ->bind('$currentUser', '%gitlab.current_user%');

        $configurator->set(ClientInterface::class, GuzzleClient::class)
            ->arg(0, [
                'base_uri' => 'https://gitlab.com',
            ]);

        $configurator->set(GitlabClient::class, GitlabClientImplementation::class)
            ->args([
                '$privateToken' => '%gitlab.private_token%',
            ]);

        $configurator->set(GitlabMergeRequestRepository::class, ApiGitlabMergeRequestRepository::class);

        $configurator->set(DomainRegistry::class, DomainRegistry::class)
            ->factory([DomainRegistry::class, 'initialize'])
            ->public()
            ->tag('app.force_initialize');

        self::configureConsoleApplication($configurator);
        self::configureReviewerPool($configurator);
    }

    private static function configureConsoleApplication(ServicesConfigurator $configurator): void
    {
        $configurator->set(MergeRequestOverview::class, MergeRequestOverview::class)
            ->arg('$name', 'merge-request:overview');

        $configurator->set(Application::class, Application::class)
            ->call('add', [new ReferenceConfigurator(MergeRequestOverview::class)])
            ->public();
    }

    private static function configureReviewerPool(ServicesConfigurator $configurator): void
    {
        $reviewerServiceIds = self::loadReviewersServices($configurator);

        $reviewerPoolService = $configurator->set(ReviewerPool::class, ReviewerPool::class);
        foreach ($reviewerServiceIds as $serviceId) {
            $reviewerPoolService->call('add', [new ReferenceConfigurator($serviceId)]);
        }
    }

    /**
     * @return string[]
     */
    private static function loadReviewersServices(ServicesConfigurator $configurator): array
    {
        $reviewerFilePath = self::CONFIG_DIRECTORY . self::REVIEWER_FILE;
        if (!\file_exists($reviewerFilePath)) {
            return [];
        }

        $reviewerFileData = \file_get_contents($reviewerFilePath);
        if ($reviewerFileData === false) {
            return [];
        }

        $reviewersData = Yaml::parse($reviewerFileData);

        if (!\array_key_exists('reviewers', $reviewersData) || !\is_array($reviewersData['reviewers'])) {
            return [];
        }

        $reviewerServiceIds = [];
        foreach ($reviewersData['reviewers'] as $reviewerData) {
            if (!\array_key_exists('username', $reviewerData) || !\array_key_exists('name', $reviewerData)) {
                continue;
            }

            $serviceId = sprintf('reviewer.%s', $reviewerData['username']);
            $configurator->set($serviceId, Reviewer::class)
                ->args([
                    '$username' => $reviewerData['username'],
                    '$name' => $reviewerData['name'],
                ]);

            $reviewerServiceIds[] = $serviceId;
        }

        return $reviewerServiceIds;
    }
}