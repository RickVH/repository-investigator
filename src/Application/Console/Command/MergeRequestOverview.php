<?php

namespace PlaceHolderX\Application\Console\Command;

use PlaceHolderX\Application\Console\Formatter\MergeRequest as MergeRequestFormatter;
use PlaceHolderX\Domain\Collection\ReviewerPool;
use PlaceHolderX\Domain\DomainRegistry;
use PlaceHolderX\Domain\ValueObject\Gitlab\ProjectIdentifier;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\ConsoleOutputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class MergeRequestOverview extends Command
{
    private const OPTION_EXCLUSION_LABEL = 'exclusion-label';
    private const OPTION_INCLUDE_WIP = 'include-wip';
    private const OPTION_NEEDED_APPROVALS = 'needed-approvals';

    private const DEFAULT_APPROVALS_NEEDED = '4';

    private string $projectId;
    private string $currentUser;
    /**
     * @var ReviewerPool
     */
    private ReviewerPool $reviewerPool;

    public function __construct(
        string $projectId,
        string $currentUser,
        ReviewerPool $reviewerPool,
        string $name = null
    )
    {
        parent::__construct($name);
        $this->projectId = $projectId;
        $this->currentUser = $currentUser;
        $this->reviewerPool = $reviewerPool;
    }

    protected function configure(): void
    {
        $this
            ->setDescription('Gets the overview of open merge requests')
            ->addOption(
                self::OPTION_EXCLUSION_LABEL,
                'e',
                InputOption::VALUE_REQUIRED|InputOption::VALUE_IS_ARRAY,
                'Excludes merge requests with given labels'
            )
            ->addOption(
                self::OPTION_INCLUDE_WIP,
                'i',
                InputOption::VALUE_NONE,
                'If set, this will include merge request with WIP or Draft status'
            )
            ->addOption(
                self::OPTION_NEEDED_APPROVALS,
                'a',
                InputOption::VALUE_REQUIRED,
                sprintf('The needed approvals a merge request needs (default is %d)', self::DEFAULT_APPROVALS_NEEDED),
                self::DEFAULT_APPROVALS_NEEDED
            )
        ;
    }

    /**
     * @param OutputInterface|ConsoleOutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        if (!$output instanceof ConsoleOutputInterface) {
            $output->writeln(sprintf('Given output interface "%s" not supported', \get_class($output)));

            return self::FAILURE;
        }

        if (!\is_numeric($input->getOption(self::OPTION_NEEDED_APPROVALS))) {
            $output->writeln(sprintf('Given option %s is not an integer value', self::OPTION_NEEDED_APPROVALS));

            return self::FAILURE;
        }

        $titleSection = $output->section();
        $titleSection->write( 'Getting open merge request overview...');

        $exclusionLabels = $input->getOption(self::OPTION_EXCLUSION_LABEL);
        $includeWip = $input->getOption(self::OPTION_INCLUDE_WIP);
        $neededApprovals = (int)$input->getOption(self::OPTION_NEEDED_APPROVALS);

        if (!\is_array($exclusionLabels)) {
            $exclusionLabels = [];
        }

        if (!\is_bool($includeWip)) {
            $includeWip = false;
        }

        $mergeRequests = DomainRegistry::instance()
            ->gitlabMergeRequestRepository()
            ->findOpenByProject(
                ProjectIdentifier::create((int)$this->projectId),
                $exclusionLabels,
                $includeWip
            );

        $titleSection->overwrite('Open merge requests:');

        if (\count($mergeRequests) === 0) {
            $output->writeln('No merge requests found with given criteria');

            return self::SUCCESS;
        }

        foreach ($mergeRequests as $mergeRequest) {
            $output->writeln(MergeRequestFormatter::format(
                $mergeRequest,
                $this->currentUser,
                $this->reviewerPool->eligibleReviewers($mergeRequest),
                $neededApprovals
            ));
        }

        return self::SUCCESS;
    }

}