<?php

declare(strict_types=1);

namespace PlaceHolderX\Application\Console\Formatter;

use PlaceHolderX\Domain\Collection\ReviewerPool;
use PlaceHolderX\Domain\Model\MergeRequest as MergeRequestModel;

final class MergeRequest
{
    private const COLOR_WHITE = 0;
    private const COLOR_GREEN = 32;
    private const COLOR_ORANGE = 33;

    public static function format(
        MergeRequestModel $mergeRequest,
        string $currentUsername,
        ReviewerPool $eligibleReviewers,
        int $neededAmountOfApprovals
    ): string
    {
        return sprintf('Approvals %d (%s) - Unresolved Discussions %d - %s (%s) - Eligible Reviewers: %s',
            $mergeRequest->amountOfApprovals(),
            self::determineApprovalSign($mergeRequest, $currentUsername, $neededAmountOfApprovals),
            $mergeRequest->amountOfUnresolvedDiscussions(),
            $mergeRequest->title(),
            $mergeRequest->url(),
            $eligibleReviewers->toString()
        );
    }

    private static function determineApprovalSign(
        MergeRequestModel $mergeRequest,
        string $currentUsername,
        int $neededAmountOfApprovals
    ): string
    {
        if ($mergeRequest->isCreatedBy($currentUsername)) {
            return sprintf("\033[%dm O \033[%dm", self::COLOR_GREEN, self::COLOR_WHITE);
        }

        if ($mergeRequest->isApprovedBy($currentUsername)) {
            return sprintf("\033[%dm X \033[%dm", self::COLOR_GREEN, self::COLOR_WHITE);
        }

        if ($mergeRequest->amountOfApprovals() >= $neededAmountOfApprovals) {
            return sprintf("\033[%dm - \033[%dm", self::COLOR_GREEN, self::COLOR_WHITE);
        }

        return sprintf("\033[%dm - \033[%dm", self::COLOR_ORANGE, self::COLOR_WHITE);
    }
}
