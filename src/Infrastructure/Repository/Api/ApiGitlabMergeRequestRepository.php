<?php

declare(strict_types=1);

namespace PlaceHolderX\Infrastructure\Repository\Api;

use PlaceHolderX\Domain\Model\MergeRequest;
use PlaceHolderX\Domain\Repository\GitlabMergeRequestRepository;
use PlaceHolderX\Domain\ValueObject\Gitlab\ProjectIdentifier;
use PlaceHolderX\Infrastructure\ApiClient\GitlabClient;
use PlaceHolderX\Infrastructure\Repository\Api\Transformer\GitlabMergeRequest as GitlabMergeRequestTransformer;
use function array_diff;
use function count;

final class ApiGitlabMergeRequestRepository implements GitlabMergeRequestRepository
{
    private GitlabClient $apiClient;

    public function __construct(GitlabClient $apiClient)
    {
        $this->apiClient = $apiClient;
    }

    /**
     * @return MergeRequest[]
     */
    public function findOpenByProject(
        ProjectIdentifier $projectIdentifier,
        array $exclusionLabels = [],
        bool $includeWip = false
    ): array
    {
        $mergeRequestsData = $this->apiClient->getOpenMergeRequests(
            $projectIdentifier->value(),
            $includeWip
        );

        $mergeRequestsData = $this->filterOutMergeRequestDataWithExclusionLabels($mergeRequestsData, $exclusionLabels);

        return array_map(function(array $mergeRequestData) {
           return GitlabMergeRequestTransformer::fromArray($this->apiClient, $mergeRequestData);
        }, $mergeRequestsData);
    }

    /**
     * @param mixed[] $mergeRequestsData
     * @param string[] $exclusionLabels
     * @return mixed[]
     */
    private function filterOutMergeRequestDataWithExclusionLabels(array $mergeRequestsData, array $exclusionLabels): array
    {
        return array_filter($mergeRequestsData, static function(array $mergeRequest) use ($exclusionLabels) {
            return count(array_diff($exclusionLabels, $mergeRequest['labels'])) === count($exclusionLabels);
        });
    }
}
