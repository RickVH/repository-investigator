<?php

declare(strict_types=1);

namespace PlaceHolderX\Infrastructure\Repository\Api\Transformer;

use PlaceHolderX\Domain\Model\Gitlab\User as GitlabUserModel;
use PlaceHolderX\Domain\ValueObject\Gitlab\UserIdentifier;
use Webmozart\Assert\Assert;

final class GitlabUser
{
    private const KEY_IDENTIFIER = 'id';
    private const KEY_USERNAME = 'username';
    private const KEY_NAME = 'name';

    /**
     * @param mixed[] $userData
     */
    public static function fromArray(array $userData): GitlabUserModel
    {
        self::assertUserData($userData);

        return new GitlabUserModel(
            UserIdentifier::create($userData[self::KEY_IDENTIFIER]),
            $userData[self::KEY_USERNAME],
            $userData[self::KEY_NAME]
        );
    }

    /**
     * @param mixed[] $userData
     */
    private static function assertUserData(array $userData): void
    {
        Assert::keyExists($userData, self::KEY_IDENTIFIER);
        Assert::keyExists($userData, self::KEY_USERNAME);
        Assert::keyExists($userData, self::KEY_NAME);

        Assert::integer($userData[self::KEY_IDENTIFIER]);
        Assert::greaterThan($userData[self::KEY_IDENTIFIER], 0);

        Assert::stringNotEmpty($userData[self::KEY_USERNAME]);

        Assert::stringNotEmpty($userData[self::KEY_NAME]);
    }
}
