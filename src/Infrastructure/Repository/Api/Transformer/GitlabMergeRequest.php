<?php

declare(strict_types=1);

namespace PlaceHolderX\Infrastructure\Repository\Api\Transformer;

use PlaceHolderX\Domain\Model\Gitlab\MergeRequest as GitlabMergeRequestModel;
use PlaceHolderX\Domain\ValueObject\Gitlab\MergeRequestIdentifier;
use PlaceHolderX\Domain\ValueObject\Gitlab\MergeRequestInternalIdentifier;
use PlaceHolderX\Domain\ValueObject\Gitlab\ProjectIdentifier;
use PlaceHolderX\Infrastructure\ApiClient\GitlabClient;
use Webmozart\Assert\Assert;
use function count;

final class GitlabMergeRequest
{
    private const KEY_IDENTIFIER = 'id';
    private const KEY_INTERNAL_IDENTIFIER = 'iid';
    private const KEY_PROJECT_IDENTIFIER = 'project_id';
    private const KEY_TITLE = 'title';
    private const KEY_WEB_URL = 'web_url';
    private const KEY_AUTHOR = 'author';

    /**
     * @param mixed[] $mergeRequestData
     */
    public static function fromArray(GitlabClient $apiClient, array $mergeRequestData): GitlabMergeRequestModel
    {
        self::assertMergeRequestData($mergeRequestData);

        $internalIdentifier = MergeRequestInternalIdentifier::create($mergeRequestData[self::KEY_INTERNAL_IDENTIFIER]);
        $projectIdentifier = ProjectIdentifier::create($mergeRequestData[self::KEY_PROJECT_IDENTIFIER]);

        $approvals = $apiClient
            ->getApprovals($projectIdentifier->value(), $internalIdentifier->value());

        $unresolvedDiscussions = $apiClient
            ->getUnresolvedDiscussions($projectIdentifier->value(), $internalIdentifier->value());

        $approvedBy = [];
        foreach ($approvals['approved_by'] as $userData) {
            $approvedBy[] = GitlabUser::fromArray($userData['user']);
        }

        return new GitlabMergeRequestModel(
            MergeRequestIdentifier::create($mergeRequestData[self::KEY_IDENTIFIER]),
            $internalIdentifier,
            $projectIdentifier,
            $mergeRequestData[self::KEY_TITLE],
            $mergeRequestData[self::KEY_WEB_URL],
            count($unresolvedDiscussions),
            $approvedBy,
            GitlabUser::fromArray($mergeRequestData[self::KEY_AUTHOR])
        );
    }

    /**
     * @param mixed[] $mergeRequestData
     */
    private static function assertMergeRequestData(array $mergeRequestData): void
    {
        Assert::keyExists($mergeRequestData, self::KEY_IDENTIFIER);
        Assert::keyExists($mergeRequestData, self::KEY_INTERNAL_IDENTIFIER);
        Assert::keyExists($mergeRequestData, self::KEY_PROJECT_IDENTIFIER);
        Assert::keyExists($mergeRequestData, self::KEY_TITLE);
        Assert::keyExists($mergeRequestData, self::KEY_WEB_URL);
        Assert::keyExists($mergeRequestData, self::KEY_AUTHOR);

        Assert::integer($mergeRequestData[self::KEY_IDENTIFIER]);
        Assert::greaterThan($mergeRequestData[self::KEY_IDENTIFIER], 0);

        Assert::integer($mergeRequestData[self::KEY_INTERNAL_IDENTIFIER]);
        Assert::greaterThan($mergeRequestData[self::KEY_INTERNAL_IDENTIFIER], 0);

        Assert::integer($mergeRequestData[self::KEY_PROJECT_IDENTIFIER]);
        Assert::greaterThan($mergeRequestData[self::KEY_PROJECT_IDENTIFIER], 0);

        Assert::stringNotEmpty($mergeRequestData[self::KEY_TITLE]);

        Assert::stringNotEmpty($mergeRequestData[self::KEY_WEB_URL]);

        Assert::isArray($mergeRequestData[self::KEY_AUTHOR]);
    }
}
