<?php

declare(strict_types=1);

namespace PlaceHolderX\Infrastructure\ApiClient\Gitlab;

use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Utils;
use PlaceHolderX\Infrastructure\ApiClient\GitlabClient;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\ResponseInterface;
use RuntimeException;
use function count;

//TODO rework exceptions handling and throwing
final class Client implements GitlabClient
{
    private ClientInterface $client;
    private string $privateToken;
    private string $version;

    public function __construct(ClientInterface $client, string $privateToken, string $version = 'v4')
    {
        $this->client = $client;
        $this->privateToken = $privateToken;
        $this->version = $version;
    }

    /**
     * @return mixed[]
     */
    private static function decodeResponseContent(ResponseInterface $response): array
    {
        $decodedJson = Utils::jsonDecode($response->getBody()->getContents(), true);

        if (!\is_array($decodedJson)) {
            return [];
        }

        return $decodedJson;
    }

    /**
     * @return mixed[]
     */
    public function getOpenMergeRequests(int $projectIdentifier, bool $includeWip = false): array
    {
        return self::decodeResponseContent($this->sendGetRequest(sprintf(
            '/api/%s/projects/%s/merge_requests?state=opened&wip=%s',
            $this->version,
            $projectIdentifier,
            $includeWip ? 'yes' : 'no'
        )));
    }

    /**
     * @return mixed[]
     */
    public function getUnresolvedDiscussions(int $projectIdentifier, int $mergeRequestInternalIdentifier, int $page = 1): array
    {
        $discussions = self::decodeResponseContent($this->sendGetRequest(sprintf(
            '/api/%s/projects/%s/merge_requests/%s/discussions?page=%d',
            $this->version,
            $projectIdentifier,
            $mergeRequestInternalIdentifier,
            $page
        )));

        $unresolvedDiscussions =  array_filter($discussions, static function(array $discussion) {
            if ($discussion['individual_note'] === true) {
                return false;
            }

            if (!$discussion['notes'][0]['resolvable']) {
                return false;
            }

            return !$discussion['notes'][0]['resolved'];
        });

        if (count($discussions) === 20) {
            $unresolvedDiscussions = array_merge($unresolvedDiscussions, $this->getUnresolvedDiscussions(
               $projectIdentifier,
               $mergeRequestInternalIdentifier,
               $page + 1
            ));
        }

        return $unresolvedDiscussions;
    }

    /**
     * @return mixed[]
     */
    public function getApprovals(int $projectIdentifier, int $mergeRequestInternalIdentifier): array
    {
        return self::decodeResponseContent($this->sendGetRequest(sprintf(
            '/api/%s/projects/%s/merge_requests/%s/approvals',
            $this->version,
            $projectIdentifier,
            $mergeRequestInternalIdentifier
        )));
    }

    private function sendGetRequest(string $url): ResponseInterface
    {
        try {
            $response = $this->client->sendRequest(new Request(
                'GET',
                $url,
                [
                    'PRIVATE-TOKEN' => $this->privateToken,
                ]
            ));
        } catch(ClientExceptionInterface $exception) {
            throw new RuntimeException('Unsuccessful response', 0, $exception);
        }

        if ($response->getStatusCode() !== 200) {
            throw new RuntimeException('Unsuccessful response');
        }

        return $response;
    }
}
