<?php

namespace PlaceHolderX\Infrastructure\ApiClient;

interface GitlabClient
{
    /**
     * @return mixed[]
     */
    public function getOpenMergeRequests(int $projectIdentifier, bool $includeWip = false): array;

    /**
     * @return mixed[]
     */
    public function getUnresolvedDiscussions(int $projectIdentifier, int $mergeRequestInternalIdentifier, int $page = 1): array;

    /**
     * @return mixed[]
     */
    public function getApprovals(int $projectIdentifier, int $mergeRequestInternalIdentifier): array;
}