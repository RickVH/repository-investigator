<?php
declare(strict_types=1);

namespace Tests\PlaceHolderX\Application\DependencyInjection\CompilerPass;

use PHPUnit\Framework\TestCase;
use PlaceHolderX\Application\DependencyInjection\CompilerPass\ForceInitialisation;
use Symfony\Component\DependencyInjection\ContainerBuilder;

final class ForceInitialisationTest extends TestCase
{
    private ContainerBuilder $mockContainerBuilder;
    private ForceInitialisation $compilerPass;

    protected function setUp(): void
    {
        $this->mockContainerBuilder = $this->createMock(ContainerBuilder::class);
        $this->compilerPass = new ForceInitialisation();
    }

    /**
     * @test
     * @throws \Exception
     */
    public function itCanProcess(): void
    {
        $this->mockContainerBuilder
            ->expects(self::once())
            ->method('findTaggedServiceIds')
            ->willReturn(['service-a' => []]);

        $this->mockContainerBuilder
            ->expects(self::once())
            ->method('get')
            ->with('service-a');

        $this->compilerPass->process($this->mockContainerBuilder);
    }
}