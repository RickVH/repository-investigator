<?php

namespace Tests\PlaceHolderX\Application\Console\Command;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use PlaceHolderX\Application\Console\Command\MergeRequestOverview;
use PlaceHolderX\Domain\Collection\ReviewerPool;
use PlaceHolderX\Domain\DomainRegistry;
use PlaceHolderX\Domain\Repository\GitlabMergeRequestRepository;
use PlaceHolderX\Domain\ValueObject\Gitlab\ProjectIdentifier;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;
use Tests\PlaceHolderX\Faker\Domain\Builder\FakeMergeRequest;

final class MergeRequestOverviewTest extends TestCase
{
    private const PROJECT_ID = '1001';
    private const CURRENT_USER = 'user-a';
    private const OUTPUT_OPTIONS = ['capture_stderr_separately' => true];

    private CommandTester $commandTester;
    /** @var MockObject */
    private MockObject $mockGitlabMergeRequestRepository;

    protected function setUp(): void
    {
        $commandName = 'merge-request:overview';
        $application = new Application();
        $application->add(new MergeRequestOverview(
            self::PROJECT_ID,
            self::CURRENT_USER,
            new ReviewerPool(),
            $commandName
        ));

        $this->commandTester = new CommandTester($application->find($commandName));

        $this->mockGitlabMergeRequestRepository = $this->createMock(GitlabMergeRequestRepository::class);

        DomainRegistry::initialize(
            $this->mockGitlabMergeRequestRepository
        );
    }

    /**
     * @test
     */
    public function itCanFetchMergeRequests(): void
    {
        $this->configureMock([[], false],[]);

        $this->commandTester->execute([], self::OUTPUT_OPTIONS);
        $output = $this->commandTester->getDisplay();

        self::assertStringContainsString('Getting open merge request overview...', $output);
        self::assertStringContainsString('Open merge requests:', $output);
        self::assertStringContainsString('No merge requests found with given criteria', $output);
    }

    /**
     * @test
     */
    public function itCanGetOwnMergeRequest(): void
    {
        $this->configureMock([[], false],[FakeMergeRequest::create()->build()]);

        $this->commandTester->execute([], self::OUTPUT_OPTIONS);
        $output = $this->commandTester->getDisplay();

        self::assertStringContainsString(
            "Approvals 0 (\e[32m O \e[0m) - Unresolved Discussions 0 - Merge Request A (https://repository-a.com/project/b/merge-request/b)",
            $output
        );
    }

    /**
     * @test
     */
    public function itCanPassAlongExclusionLabels(): void
    {
        $this->configureMock([[
            'Blocked'
        ], false],[]);

        $commandResultCode = $this->commandTester->execute([
            '--exclusion-label' => ['Blocked']
        ], self::OUTPUT_OPTIONS);

        self::assertSame(0, $commandResultCode);
    }

    /**
     * @test
     */
    public function itCanPassAlongIncludeWip(): void
    {
        $this->configureMock([[], true],[]);

        $commandResultCode = $this->commandTester->execute([
            '--include-wip' => null
        ], self::OUTPUT_OPTIONS);

        self::assertSame(0, $commandResultCode);
    }

    /**
     * @test
     */
    public function itCanPassAlongApprovalsNeeded(): void
    {
        $this->configureMock([[], false],[
            FakeMergeRequest::create()
                ->withCreatedBy('user-b')
                ->build(),
            FakeMergeRequest::create()
                ->withTitle('Merge Request B')
                ->withCreatedBy('user-b')
                ->addApproval('user-c')
                ->build(),
        ]);

        $this->commandTester->execute([
            '--needed-approvals' => 1
        ], self::OUTPUT_OPTIONS);

        $output = $this->commandTester->getDisplay();

        self::assertStringContainsString(
            "Approvals 0 (\e[33m - \e[0m) - Unresolved Discussions 0 - Merge Request A (https://repository-a.com/project/b/merge-request/b)",
            $output
        );
        self::assertStringContainsString(
            "Approvals 1 (\e[32m - \e[0m) - Unresolved Discussions 0 - Merge Request B (https://repository-a.com/project/b/merge-request/b)",
            $output
        );
    }

    private function configureMock(array $withArguments, array $returnValues):  void
    {
        $this->mockGitlabMergeRequestRepository
            ->expects(self::once())
            ->method('findOpenByProject')
            ->with(ProjectIdentifier::create((int)self::PROJECT_ID), ...$withArguments)
            ->willReturn($returnValues);
    }
}