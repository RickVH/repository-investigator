<?php
declare(strict_types=1);

namespace Tests\PlaceHolderX\Application\Console\Formatter;

use PHPUnit\Framework\TestCase;
use PlaceHolderX\Application\Console\Formatter\MergeRequest as MergeRequestFormatter;
use PlaceHolderX\Domain\Collection\ReviewerPool;
use Tests\PlaceHolderX\Faker\Domain\Builder\FakeMergeRequest as FakeMergeRequestBuilder;

final class MergeRequestTest extends TestCase
{
    /**
     * @test
     */
    public function itCanFormatOwnMergeRequest(): void
    {
        $formatted = MergeRequestFormatter::format(
            FakeMergeRequestBuilder::create()->build(),
            'user-a',
            new ReviewerPool(),
            4
        );

        self::assertSame(
            "Approvals 0 (\033[32m O \033[0m) - Unresolved Discussions 0 - Merge Request A (https://repository-a.com/project/b/merge-request/b) - Eligible Reviewers: None",
            $formatted
        );
    }

    /**
     * @test
     */
    public function itCanFormatOwnMergeRequestWithApprovals(): void
    {
        $formatted = MergeRequestFormatter::format(
            FakeMergeRequestBuilder::create()
                ->addApproval('user-b')
                ->addApproval('user-c')
                ->build(),
            'user-a',
            new ReviewerPool(),
            4
        );

        self::assertSame(
            "Approvals 2 (\033[32m O \033[0m) - Unresolved Discussions 0 - Merge Request A (https://repository-a.com/project/b/merge-request/b) - Eligible Reviewers: None",
            $formatted
        );
    }

    /**
     * @test
     */
    public function itCanFormatOwnMergeRequestWithUnresolvedDiscussions(): void
    {
        $formatted = MergeRequestFormatter::format(
            FakeMergeRequestBuilder::create()
                ->addUnresolvedDiscussion()
                ->addUnresolvedDiscussion()
                ->addUnresolvedDiscussion()
                ->build(),
            'user-a',
            new ReviewerPool(),
            4
        );

        self::assertSame(
            "Approvals 0 (\033[32m O \033[0m) - Unresolved Discussions 3 - Merge Request A (https://repository-a.com/project/b/merge-request/b) - Eligible Reviewers: None",
            $formatted
        );
    }

    /**
     * @test
     */
    public function itCanFormatOtherMergeRequestWithOwnApproval(): void
    {
        $formatted = MergeRequestFormatter::format(
            FakeMergeRequestBuilder::create()
                ->addApproval('user-b')
                ->build(),
            'user-b',
            new ReviewerPool(),
            4
        );

        self::assertSame(
            "Approvals 1 (\033[32m X \033[0m) - Unresolved Discussions 0 - Merge Request A (https://repository-a.com/project/b/merge-request/b) - Eligible Reviewers: None",
            $formatted
        );
    }

    /**
     * @test
     */
    public function itCanFormatOtherMergeRequestWithOtherApproval(): void
    {
        $formatted = MergeRequestFormatter::format(
            FakeMergeRequestBuilder::create()
                ->addApproval('user-c')
                ->build(),
            'user-b',
            new ReviewerPool(),
            4
        );

        self::assertSame(
            "Approvals 1 (\033[33m - \033[0m) - Unresolved Discussions 0 - Merge Request A (https://repository-a.com/project/b/merge-request/b) - Eligible Reviewers: None",
            $formatted
        );
    }
}