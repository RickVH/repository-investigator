<?php
declare(strict_types=1);

namespace Tests\PlaceHolderX\Infrastructure\Repository\Api\Transformer;

use PHPUnit\Framework\TestCase;
use PlaceHolderX\Infrastructure\Repository\Api\Transformer\GitlabUser as GitlabUserTransformer;

final class GitlabUserTest extends TestCase
{
    /**
     * @test
     */
    public function itCanTransformFromArray(): void
    {
        $model = GitlabUserTransformer::fromArray([
            'id' => 10,
            'username' => 'user-a',
            'name' => 'User A',
        ]);

        self::assertSame(10, $model->identifier()->value());
        self::assertSame('user-a', $model->username());
        self::assertSame('User A', $model->name());
    }

    /**
     * @test
     * @dataProvider invalidUserDataProvider
     */
    public function itThrowsExceptionOnInvalidData(array $invalidUserData, string $exceptionMessage): void
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage($exceptionMessage);

        GitlabUserTransformer::fromArray($invalidUserData);
    }

    public function invalidUserDataProvider(): \Generator
    {
        yield [[], 'Expected the key "id" to exist.'];

        yield [['id' => 0], 'Expected the key "username" to exist.'];

        yield [['id' => 0, 'username' => ''], 'Expected the key "name" to exist.'];

        yield [['id' => 0, 'username' => '', 'name' => ''], 'Expected a value greater than 0. Got: 0'];

        yield [['id' => 1, 'username' => '', 'name' => ''], 'Expected a different value than "".'];

        yield [['id' => 1, 'username' => 'user-a', 'name' => ''], 'Expected a different value than "".'];
    }
}