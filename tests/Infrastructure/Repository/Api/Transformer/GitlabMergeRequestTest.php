<?php
declare(strict_types=1);

namespace Tests\PlaceHolderX\Infrastructure\Repository\Api\Transformer;

use PHPUnit\Framework\TestCase;
use PlaceHolderX\Infrastructure\Repository\Api\Transformer\GitlabMergeRequest as GitlabMergeRequestTransformer;
use Tests\PlaceHolderX\Faker\Infrastructure\ApiClient\Gitlab\FakeClient;

final class GitlabMergeRequestTest extends TestCase
{
    private FakeClient $fakeClient;

    protected function setUp(): void
    {
        $this->fakeClient = new FakeClient();
    }

    /**
     * @test
     */
    public function itCanTransformFromArray(): void
    {
        $mergeRequest = GitlabMergeRequestTransformer::fromArray(
            $this->fakeClient, $this->fakeClient->mergeRequests[10]
        );

        self::assertSame(10, $mergeRequest->identifier()->value());
        self::assertSame(15, $mergeRequest->internalIdentifier()->value());
        self::assertSame(20, $mergeRequest->projectIdentifier()->value());
        self::assertSame('A merge request', $mergeRequest->title());
        self::assertSame('https://www.gitlab.com/project/20/merge_request/15', $mergeRequest->url());
        self::assertSame(0, $mergeRequest->amountOfApprovals());
        self::assertSame(0, $mergeRequest->amountOfUnresolvedDiscussions());
    }

    /**
     * @test
     */
    public function itCanTransformFromArrayWithApprovals(): void
    {
        $mergeRequest = GitlabMergeRequestTransformer::fromArray(
            $this->fakeClient, $this->fakeClient->mergeRequests[11]
        );

        self::assertSame(2, $mergeRequest->amountOfApprovals());
    }

    /**
     * @test
     */
    public function itCanTransformFromArrayWithUnresolvedDiscussions(): void
    {
        $mergeRequest = GitlabMergeRequestTransformer::fromArray(
            $this->fakeClient, $this->fakeClient->mergeRequests[12]
        );

        self::assertSame(3, $mergeRequest->amountOfUnresolvedDiscussions());
    }

    /**
     * @test
     * @dataProvider invalidUserDataProvider
     */
    public function itThrowsExceptionOnInvalidData(array $invalidMergeRequestData, string $exceptionMessage): void
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage($exceptionMessage);

        GitlabMergeRequestTransformer::fromArray($this->fakeClient, $invalidMergeRequestData);
    }

    public function invalidUserDataProvider(): \Generator
    {
        yield [[], 'Expected the key "id" to exist.'];
        yield [['id' => 0], 'Expected the key "iid" to exist.'];
        yield [['id' => 0, 'iid' => 0], 'Expected the key "project_id" to exist.'];
        yield [['id' => 0, 'iid' => 0, 'project_id' => 0], 'Expected the key "title" to exist.'];
        yield [['id' => 0, 'iid' => 0, 'project_id' => 0, 'title' => ''], 'Expected the key "web_url" to exist.'];
        yield [
            ['id' => 0, 'iid' => 0, 'project_id' => 0, 'title' => '', 'web_url' => ''],
            'Expected the key "author" to exist.'
        ];
        yield [
            ['id' => 0, 'iid' => 0, 'project_id' => 0, 'title' => '', 'web_url' => '', 'author' => ''],
            'Expected a value greater than 0. Got: 0'
        ];
        yield [
            ['id' => 1, 'iid' => 0, 'project_id' => 0, 'title' => '', 'web_url' => '', 'author' => ''],
            'Expected a value greater than 0. Got: 0'
        ];
        yield [
            ['id' => 1, 'iid' => 2, 'project_id' => 0, 'title' => '', 'web_url' => '', 'author' => ''],
            'Expected a value greater than 0. Got: 0'
        ];
        yield [
            ['id' => 1, 'iid' => 2, 'project_id' => 3, 'title' => '', 'web_url' => '', 'author' => ''],
            'Expected a different value than "".'
        ];
        yield [
            ['id' => 1, 'iid' => 2, 'project_id' => 3, 'title' => 'Title', 'web_url' => '', 'author' => ''],
            'Expected a different value than "".'
        ];
        yield [
            ['id' => 1, 'iid' => 2, 'project_id' => 3, 'title' => 'Title', 'web_url' => 'https://test.com', 'author' => ''],
            'Expected an array. Got: string'
        ];
    }

}
