<?php

declare(strict_types=1);

namespace Tests\PlaceHolderX\Infrastructure\Repository\Api;

use PHPUnit\Framework\TestCase;
use PlaceHolderX\Domain\ValueObject\Gitlab\ProjectIdentifier;
use PlaceHolderX\Infrastructure\Repository\Api\ApiGitlabMergeRequestRepository;
use Tests\PlaceHolderX\Faker\Infrastructure\ApiClient\Gitlab\FakeClient;

final class ApiGitlabMergeRequestRepositoryTest extends TestCase
{
    private ApiGitlabMergeRequestRepository $repository;

    protected function setUp(): void
    {
        $fakeClient = new FakeClient();
        $this->repository = new ApiGitlabMergeRequestRepository($fakeClient);
    }

    /**
     * @test
     */
    public function itCanFindOpenByProjectIdentifier():void
    {
        $projectIdentifier = ProjectIdentifier::create(20);

        $mergeRequests = $this->repository->findOpenByProject($projectIdentifier);

        self::assertCount(3, $mergeRequests);
    }

    /**
     * @test
     */
    public function itCanFilterOutCertainLabels():void
    {
        $projectIdentifier = ProjectIdentifier::create(20);

        $mergeRequests = $this->repository->findOpenByProject($projectIdentifier, ['not important']);

        self::assertCount(2, $mergeRequests);
    }
}
