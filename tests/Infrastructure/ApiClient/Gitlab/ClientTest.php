<?php

namespace Tests\PlaceHolderX\Infrastructure\ApiClient\Gitlab;

use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Utils;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use PlaceHolderX\Infrastructure\ApiClient\Gitlab\Client as GitlabClient;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;
use RuntimeException;

final class ClientTest extends TestCase
{
    private GitlabClient $client;
    /** @var MockObject */
    private MockObject $mockHttpClient;
    private string $privateToken;
    /** @var Request[] */
    private array $requests;

    protected function setUp(): void
    {
        $this->privateToken = 'super-private-token';
        $this->requests = [];
        $this->mockHttpClient = $this->createMock(ClientInterface::class);
        $this->client = new GitlabClient(
            $this->mockHttpClient,
            $this->privateToken
        );
    }

    /**
     * @test
     */
    public function itCanGetOpenMergeRequests(): void
    {
        $projectIdentifier = 10001;

        $this->mockHttpClient
            ->expects(self::once())
            ->method('sendRequest')
            ->willReturnCallback(function(Request $request) {
                $this->requests[] = $request;
                return new Response(200, [], '[]');
            });

        $this->client->getOpenMergeRequests($projectIdentifier);

        self::assertCount(1, $this->requests);
        $this->assertRequest(
            array_shift($this->requests),
            '/api/v4/projects/10001/merge_requests',
            'state=opened&wip=no'
        );
    }

    /**
     * @test
     */
    public function itCanGetApprovals(): void
    {
        $projectIdentifier = 10001;
        $mergeRequestInternalIdentifier = 10;

        $this->mockHttpClient
            ->expects(self::once())
            ->method('sendRequest')
            ->willReturnCallback(function(Request $request) {
                $this->requests[] = $request;
                return new Response(200, [], '[]');
            });

        $this->client->getApprovals($projectIdentifier, $mergeRequestInternalIdentifier);

        self::assertCount(1, $this->requests);
        $this->assertRequest(
            array_shift($this->requests),
            '/api/v4/projects/10001/merge_requests/10/approvals',
            ''
        );
    }

    /**
     * @test
     */
    public function itCanGetUnresolvedDiscussions(): void
    {
        $projectIdentifier = 10001;
        $mergeRequestInternalIdentifier = 10;

        $this->mockHttpClient
            ->expects(self::once())
            ->method('sendRequest')
            ->willReturnCallback(function(Request $request) {
                $this->requests[] = $request;
                return new Response(200, [], Utils::jsonEncode([
                    ['individual_note' => true,],
                    ['individual_note' => false, 'notes' => [['resolvable' => false,]]],
                    ['individual_note' => false, 'notes' => [['resolvable' => true, 'resolved' => true]]],
                    $this->getUnresolvedDiscussionData(),
                ]));
            });

        $unresolvedDiscussion = $this->client->getUnresolvedDiscussions(
            $projectIdentifier,
            $mergeRequestInternalIdentifier
        );

        self::assertCount(1, $this->requests);
        self::assertCount(1, $unresolvedDiscussion);
        $this->assertRequest(
            array_shift($this->requests),
            '/api/v4/projects/10001/merge_requests/10/discussions',
            'page=1'
        );
    }

    /**
     * @test
     */
    public function itCanGetMultiPagesOfUnresolvedDiscussions(): void
    {
        $projectIdentifier = 10001;
        $mergeRequestInternalIdentifier = 10;

        $this->mockHttpClient
            ->expects(self::exactly(2))
            ->method('sendRequest')
            ->willReturnCallback(function(Request $request) {
                $this->requests[] = $request;

                if (count($this->requests) === 1) {
                    $unresolvedDiscussions = [];
                    for ($i = 0; $i < 20; $i++) {
                        $unresolvedDiscussions[] = $this->getUnresolvedDiscussionData();
                    }

                    return new Response(200, [], Utils::jsonEncode($unresolvedDiscussions));
                }

                return new Response(200, [], Utils::jsonEncode([
                    $this->getUnresolvedDiscussionData(),
                ]));
            });

        $unresolvedDiscussion = $this->client->getUnresolvedDiscussions(
            $projectIdentifier,
            $mergeRequestInternalIdentifier
        );

        self::assertCount(2, $this->requests);
        self::assertCount(21, $unresolvedDiscussion);
        $this->assertRequest(
            array_shift($this->requests),
            '/api/v4/projects/10001/merge_requests/10/discussions',
            'page=1'
        );
        $this->assertRequest(
            array_shift($this->requests),
            '/api/v4/projects/10001/merge_requests/10/discussions',
            'page=2'
        );
    }

    /**
     * @test
     */
    public function itThrowsRunTimeExceptionOnUnsuccessfulResponse(): void
    {
        $this->mockHttpClient
            ->expects(self::once())
            ->method('sendRequest')
            ->willReturn(new Response(500, [], 'Totally broken'));

        $this->expectExceptionObject(new RuntimeException('Unsuccessful response'));

        $this->client->getOpenMergeRequests(10001);
    }

    /**
     * @test
     */
    public function itThrowsRunTimeExceptionOnClientException(): void
    {
        $clientException = $this->createMock(ClientExceptionInterface::class);
        $this->mockHttpClient
            ->expects(self::once())
            ->method('sendRequest')
            ->willThrowException($clientException);

        $this->expectExceptionObject(new RuntimeException('Unsuccessful response', 0, $clientException));

        $this->client->getOpenMergeRequests(10001);
    }

    private function assertRequest(Request $request, string $uriPath, string $uriQuery): void
    {
        self::assertSame('GET', $request->getMethod());
        self::assertSame($uriPath, $request->getUri()->getPath());
        self::assertSame($uriQuery, $request->getUri()->getQuery());
        self::assertSame([$this->privateToken], $request->getHeader('PRIVATE-TOKEN'));
    }

    private function getUnresolvedDiscussionData(): array
    {
        return [
            'individual_note' => false,
            'notes' => [
                [
                    'resolvable' => true,
                    'resolved' => false
                ]
            ]
        ];
    }
}