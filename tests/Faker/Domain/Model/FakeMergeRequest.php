<?php
declare(strict_types=1);

namespace Tests\PlaceHolderX\Faker\Domain\Model;

use PlaceHolderX\Domain\Model\MergeRequest;

final class FakeMergeRequest implements MergeRequest
{
    private string $title;
    private string $url;
    private string $createdBy;
    private array $unresolvedDiscussions;
    private array $approvals;

    public function __construct(
        string $title,
        string $url,
        string $createdBy,
        array $unresolvedDiscussions,
        array $approvals
    )
    {
        $this->title = $title;
        $this->url = $url;
        $this->createdBy = $createdBy;
        $this->unresolvedDiscussions = $unresolvedDiscussions;
        $this->approvals = $approvals;
    }

    public function title(): string
    {
        return $this->title;
    }

    public function url(): string
    {
        return $this->url;
    }

    public function amountOfUnresolvedDiscussions(): int
    {
        return \count($this->unresolvedDiscussions);
    }

    public function amountOfApprovals(): int
    {
        return \count($this->approvals);
    }

    public function isApprovedBy(string $username): bool
    {
        return \in_array($username, $this->approvals, true);
    }

    public function isCreatedBy(string $username): bool
    {
        return $this->createdBy === $username;
    }
}