<?php
declare(strict_types=1);

namespace Tests\PlaceHolderX\Faker\Domain\Builder;

use Tests\PlaceHolderX\Faker\Domain\Model\FakeMergeRequest as FakeMergeRequestModel;

final class FakeMergeRequest
{
    private string $title;
    private string $url;
    private string $createdBy;
    private array $unresolvedDiscussions;
    private array $approvals;

    private function __construct()
    {
        $this->title = 'Merge Request A';
        $this->url = 'https://repository-a.com/project/b/merge-request/b';
        $this->createdBy = 'user-a';
        $this->unresolvedDiscussions = [];
        $this->approvals = [];
    }

    public static function create(): self
    {
        return new self();
    }

    public function withTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function withCreatedBy(string $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function addApproval(string $approvalUsername): self
    {
        $this->approvals[] = $approvalUsername;

        return $this;
    }

    public function addUnresolvedDiscussion(): self
    {
        $this->unresolvedDiscussions[] = [];

        return $this;
    }

    public function build(): FakeMergeRequestModel
    {
        return new FakeMergeRequestModel(
            $this->title,
            $this->url,
            $this->createdBy,
            $this->unresolvedDiscussions,
            $this->approvals
        );
    }
}