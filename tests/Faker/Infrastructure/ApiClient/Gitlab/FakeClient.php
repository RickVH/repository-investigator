<?php

declare(strict_types=1);

namespace Tests\PlaceHolderX\Faker\Infrastructure\ApiClient\Gitlab;

use PlaceHolderX\Infrastructure\ApiClient\GitlabClient;

final class FakeClient implements GitlabClient
{
    public array $mergeRequests = [
        10 => [
            'id' => 10,
            'iid' => 15,
            'project_id' => 20,
            'title' => 'A merge request',
            'web_url' => 'https://www.gitlab.com/project/20/merge_request/15',
            'labels' => [],
            'author' => [
                'id' => 25,
                'username' => 'user-a',
                'name' => 'User A',
            ],
            'wip' => false
        ],
        11 => [
            'id' => 11,
            'iid' => 16,
            'project_id' => 20,
            'title' => 'Another merge request',
            'web_url' => 'https://www.gitlab.com/project/20/merge_request/16',
            'labels' => [
                'not important'
            ],
            'author' => [
                'id' => 25,
                'username' => 'user-a',
                'name' => 'User A',
            ],
            'wip' => false
        ],
        12 => [
            'id' => 12,
            'iid' => 17,
            'project_id' => 20,
            'title' => 'A third merge request',
            'web_url' => 'https://www.gitlab.com/project/20/merge_request/17',
            'labels' => [],
            'author' => [
                'id' => 25,
                'username' => 'user-a',
                'name' => 'User A',
            ],
            'wip' => false
        ],
    ];

    private array $approvals = [
        '20-16' => [
            'approved_by' => [
                [
                    'user' => [
                        'id' => 25,
                        'username' => 'user-a',
                        'name' => 'User A',
                    ],
                ],
                [
                    'user' => [
                        'id' => 30,
                        'username' => 'user-b',
                        'name' => 'User B',
                    ],
                ],
            ],
        ]
    ];

    private array $discussions = [
        '20-17' => [
            [], [], []
        ]
    ];

    public function getOpenMergeRequests(int $projectIdentifier, bool $includeWip = false): array
    {
        $mergeRequests = [];
        foreach ($this->mergeRequests as $mergeRequest) {
            if (!$includeWip && $mergeRequest['wip']) {
                continue;
            }

            if ($mergeRequest['project_id'] !== $projectIdentifier) {
                continue;
            }

            $mergeRequests[] = $mergeRequest;
        }

        return $mergeRequests;
    }

    public function getUnresolvedDiscussions(int $projectIdentifier, int $mergeRequestInternalIdentifier, int $page = 1): array
    {
        $key = sprintf('%d-%d', $projectIdentifier, $mergeRequestInternalIdentifier);
        if (!\array_key_exists($key, $this->discussions)) {
            return [];
        }

        return $this->discussions[$key];
    }

    public function getApprovals(int $projectIdentifier, int $mergeRequestInternalIdentifier): array
    {
        $key = sprintf('%d-%d', $projectIdentifier, $mergeRequestInternalIdentifier);
        if (!\array_key_exists($key, $this->approvals)) {
            return [
                'approved_by' => []
            ];
        }

        return $this->approvals[$key];
    }
}
